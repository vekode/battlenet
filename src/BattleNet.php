<?php

namespace Vekode\BattleNet;

use Vekode\BattleNet\Traits\CharacterClassesTrait;
use Vekode\BattleNet\Traits\CharacterRacesTrait;

class BattleNet {

    use CharacterClassesTrait;
    use CharacterRacesTrait;

    protected $api_url;

    public function __construct()
    {
        $this->api_url = config('vbnet.API_URL');
    }

    public function hello()
    {
        return "Hello";
    }

}