<?php

namespace Vekode\BattleNet\Traits;

use GuzzleHttp;

Trait CharacterClassesTrait {

    public function CharacterClasses()
    {

        $client = new GuzzleHttp\Client(['base_uri' => $this->api_url]);

        $response = $client->get('/wow/data/character/classes?locale=en_US&apikey=45ny4bbq8wsubyvjsf8whvav4ftj85ep');

        return $response->getBody();

    }


}