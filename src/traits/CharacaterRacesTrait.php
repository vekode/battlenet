<?php

namespace Vekode\BattleNet\Traits;

use GuzzleHttp;

Trait CharacterRacesTrait {

    public function CharacterRaces()
    {
        $client = new GuzzleHttp\Client(['base_uri' => $this->api_url]);
        $response = $client->get('/wow/data/character/races?locale=en_US&apikey=45ny4bbq8wsubyvjsf8whvav4ftj85ep');
        return $response->getBody();

    }

    public function RaceNames()
    {
        $races = $this->CharacterRaces();

        $races = json_decode($races, true);

        collect($races);

        $race_names = [];

        foreach($races as $array)
        {
            foreach($array as $race)
            {
                $race_names[] = $race['name'];
            }
        }

        $result = array_unique($race_names);

        return $result;
    }


}